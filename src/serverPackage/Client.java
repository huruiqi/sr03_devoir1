package serverPackage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;

/**
 * Client DataUnit de ArrayList
 *
 * @author huruiqi wanghongzhe
 * @date 2023/03/26
 */
class Client implements Runnable {
    private final ChatServer chatServer;
    private String name = ""; // le nom de ce client
    private Socket s; // le socket utilisé pour communiquer avec le client
    private DataInputStream dis = null; // le flux d'entrée pour recevoir les messages du client
    private boolean bConnected = false; // un indicateur pour savoir si le client est connecté
    private DataOutputStream dos = null; // le flux de sortie pour envoyer les messages au client

    public String getName() {
        return name;
    }

    public DataInputStream getDis() {
        return dis;
    }

    public DataOutputStream getDos() {
        return dos;
    }

    public Socket getS() {
        return s;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Client, DataUnit d'Arraylist
     * Initialisation des flux de données pour recevoir et envoyer les messages
     * @param chatServer ChatServer
     * @param s          Socket
     */
    public Client(ChatServer chatServer, Socket s) {
        this.chatServer = chatServer;
        this.s = s;
        try {
            dis = new DataInputStream(s.getInputStream());
            dos = new DataOutputStream(s.getOutputStream());
            bConnected = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode pour envoyer un message à ce client
     *
     * @param str str le message à envoyer
     */
    public void send(String str) {
        try {
            dos.writeUTF(str);
        } catch (IOException e) {
            chatServer.clients.remove(this);
            //System.out.println("对方退出了，我从list里面去掉了！");
        }
    }

    /**
     * Implémenter la méthode run pour exécuter un thread pour le traitement d'un seul client
     */
    public void run() {
        try {
            while (true) {
                //le code lit le nom d'utilisateur à partir du flux d'entrée et vérifie si ce nom d'utilisateur est déjà utilisé par un autre client.
                Integer flag = 0;
                String str_tmp = getDis().readUTF();

                for (int i = 0; i < chatServer.clients.size(); i++) {
                    //System.out.println(chatServer.clients.get(i).getName());
                    if (str_tmp.equals(chatServer.clients.get(i).getName())) {
                        flag = 1;
                    }
                }
                //Si le nom d'utilisateur n'est pas déjà utilisé, il est défini sur le nom du client et un message est envoyé à tous les clients indiquant que le client a rejoint la session.
                if (flag == 0) {
                    getDos().writeUTF(str_tmp);
                    System.out.println(str_tmp + " a rejoint la conservation");
                    for (int i = 0; i < chatServer.clients.size(); i++) {
                        Client c1 = chatServer.clients.get(i);
                        if (!c1.getName().equals("")) {
                            c1.send(str_tmp + " a rejoint la conservation");
                        }
                    }
                    setName(str_tmp);
                    break;
                } else {
                    //Si le nom d'utilisateur est déjà pris, envoyez un "faux" message au client, indiquant que le nom d'utilisateur est déjà pris.
                    getDos().writeUTF("false");
                }
            }
            // dis.close();

            //le code continue de lire un message du client à partir du flux d'entrée et envoie ce message à tous les autres clients.
            while (bConnected) {
                String str = dis.readUTF();
                for (int i = 0; i < chatServer.clients.size(); i++) {
                    Client c = chatServer.clients.get(i);
                    c.send(this.name + " a dit " + str);
                }
            }
        } catch (EOFException e) {
            //Si une EOFException se produit, cela signifie que le client a quitté la session, cette méthode supprimera le client de la liste des clients et enverra un message aux autres clients indiquant que le client a quitté la session.
            if (getName().equals("")) {
                chatServer.clients.remove(this);
                System.out.println(this.name + " un client anonyme quitté la conversation");
            } else {
                System.out.println(this.name + " a quitté la conversation");
                chatServer.clients.remove(this);
                for (int i = 0; i < chatServer.clients.size(); i++) {
                    Client c = chatServer.clients.get(i);
                    c.send(this.name + " a quitté la conversation");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //Enfin, la méthode ferme tous les flux et sockets.
            try {
                if (dis != null)
                    dis.close();
                if (dos != null)
                    dos.close();
                if (s != null)
                    s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
