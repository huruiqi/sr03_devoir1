package serverPackage;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Serveur de discussion
 *
 * Cette classe représente le serveur de discussion qui permet à plusieurs clients de se connecter
 * et de discuter ensemble en temps réel.
 *
 * @author huruiqi wanghongzhe
 * @date 2023/03/26
 */
public class ChatServer {
    boolean started = false; //Variable booléenne qui identifie si la connexion est normalement ouverte
    ServerSocket ss = null; //Socket du serveur

    // Liste de clients
    volatile CopyOnWriteArrayList<Client> clients = new CopyOnWriteArrayList<>();

    public static void main(String Args[]) {

        new ChatServer().start(); //Instanciation d'un nouvel objet ChatServer et appel de la méthode start()

    }

    /**
     * Démarre le serveur
     *
     * Cette méthode initialise le socket du serveur, crée un nouveau client pour chaque connexion
     * et démarre un nouveau thread pour chaque client.
     */
    public void start() {
        try {
            //Initialisation du socket du serveur
            ss = new ServerSocket(9999); // Port de TCP
            started = true; //Le serveur a démarré avec succès

        } catch (BindException e) {
            //Le port est déjà utilisé, on affiche un message d'erreur et on arrête le programme
            System.out.println("Ce port est déjà utilisé");
            System.exit(0);
        } catch (IOException e) {
            //Une exception s'est produite lors de l'initialisation du socket du serveur
            e.printStackTrace();
        }
        Client c = null;
        try {
            //On boucle tant que le serveur est démarré
            while (started) {
                try {
                    //Attente d'une nouvelle connexion entrante
                    Socket s = ss.accept();
                    //Création d'un nouveau client pour la connexion entrante
                    c = new Client(this, s);
                    //Démarrage d'un nouveau thread pour le client
                    new Thread(c).start();
                    //Ajout du client à la liste des clients
                    clients.add(c);
                } catch (IOException e) {
                    //Une exception s'est produite lors de l'établissement de la connexion
                    e.printStackTrace();
                    // dis.close();
                }
            }
        } finally {
            try {
                //Fermeture du socket du serveur
                ss.close();
            } catch (IOException e) {
                //Une exception s'est produite lors de la fermeture du socket du serveur
                e.printStackTrace();
            }

        }
    }
}