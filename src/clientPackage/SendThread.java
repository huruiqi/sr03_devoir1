package clientPackage;

import java.io.IOException;

/**
 * Classe SendThread
 *
 * Cette classe est responsable de l'envoi des messages.
 */
class SendThread implements Runnable {
    private final ChatClient chatClient;

    /**
     * Constructeur SendThread
     *
     * Ce constructeur prend un objet ChatClient en paramètre et l'assigne à la variable d'instance chatClient.
     *
     * @param chatClient    l'objet ChatClient associé à cette instance de SendThread
     */
    public SendThread(ChatClient chatClient) {
        this.chatClient = chatClient;
    }

    /**
     * Méthode run
     *
     * Cette méthode est appelée lorsqu'un thread est démarré. Elle est responsable de l'envoi des messages.
     */
    public void run() {
        while (chatClient.isbConnected()) {
            try {
                String str = chatClient.scanner.next();

                // Si l'utilisateur entre la commande "Quit", on termine la connexion et on sort de la boucle
                if (str.equals("Quit")) {
                    System.out.println("Bye");
                    chatClient.disconnect();
                }

                // On envoie le message au serveur
                chatClient.dos.writeUTF(str);
            } catch (IOException e) {
                // En cas d'erreur, on affiche la pile d'erreurs
                e.printStackTrace();
            }

        }
    }
}
