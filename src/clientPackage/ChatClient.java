package clientPackage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**

 Classe du client de chat
 @author huruiqi wanghongzhe
 @date 2023/03/26
 */
public class ChatClient {
    Socket s = null; // Déclare un objet Socket pour gérer la connexion
    Scanner scanner = new Scanner(System.in); // Déclare un objet Scanner pour lire l'entrée utilisateur
    DataOutputStream dos; // Déclare un objet DataOutputStream pour envoyer des données via la connexion
    DataInputStream dis; // Déclare un objet DataInputStream pour recevoir des données via la connexion
    private boolean bConnected = false; // Un drapeau pour indiquer si le client est connecté
    Thread tSend = new Thread(new SendThread(this)); // Déclare un thread pour envoyer des messages
    Thread tRecv = new Thread(new RecvThread(this)); // Déclare un thread pour recevoir des messages

    /**
     Méthode pour vérifier si le client est connecté
     @return un booléen indiquant si le client est connecté
     */
    public boolean isbConnected() {
        return bConnected;
    }

    /**
     Méthode principale pour lancer le client
     */
    public static void main(String[] args) {
        new ChatClient().launch(); // Instancie un nouveau client et appelle la méthode de lancement
    }

    /**
     Méthode de lancement du client
     */
    public void launch() {
        connect(); // Établit une connexion avec le serveur
        while (true) { // Boucle infinie pour gérer la connexion
            System.out.println("Enterer votre pseudo"); // Demande à l'utilisateur d'entrer son pseudo
            String str = scanner.next(); // Lit l'entrée utilisateur
            try {
                if (str != null) {
                    if (str.equals("Quit")) { // Si l'utilisateur entre "Quit"
                        System.out.println("Bye"); // Affiche "Bye"
                        disconnect(); // Déconnecte le client
                    }
                    dos.writeUTF(str); // Envoie le pseudo au serveur via la connexion
                    if (!dis.readUTF().equals("false")) { // Si le serveur a accepté le pseudo
                        System.out.println("Welcome"); // Affiche "Welcome"
                        break; // Sort de la boucle d'authentification
                    } else {
                        System.out.println("Pseudo deja existe"); // Sinon, affiche "Pseudo deja existe"
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace(); // Affiche les erreurs d'E/S
            }
        }
        tSend.start(); // Lance le thread pour envoyer des messages
        tRecv.start(); // Lance le thread pour recevoir des messages
    }

    /**
     * Connecter au serveur
     */
    public void connect() {
        try {
            s = new Socket("127.0.0.1", 9999); // créer un nouveau socket avec l'adresse IP et le port du serveur
            dos = new DataOutputStream(s.getOutputStream()); // obtenir le flux de sortie pour envoyer des données au serveur
            dis = new DataInputStream(s.getInputStream()); // obtenir le flux d'entrée pour recevoir les données du serveur
            System.out.println("Connected, entrer 'Quit' pour quitter"); // afficher un message de connexion réussie
            bConnected = true;

        } catch (UnknownHostException e) {
            e.printStackTrace(); // afficher la trace de l'erreur
        } catch (ConnectException e) {
            System.out.println("Le Serveur est ferme."); // afficher un message d'erreur si le serveur est fermé
            System.exit(0); // fermer l'application
        } catch (IOException e) {
            e.printStackTrace(); // afficher la trace de l'erreur
        }
    }

    /**
     * Déconnecter du serveur
     */
    public void disconnect() {
        try {
            dos.close(); // fermer le flux de sortie
            dis.close(); // fermer le flux d'entrée
            s.close(); // fermer le socket
            bConnected=false;
            System.exit(0); // fermer l'application
        } catch (IOException e) {
            e.printStackTrace(); // afficher la trace de l'erreur
        }
    }
}

