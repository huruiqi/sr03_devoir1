package clientPackage;

import java.io.EOFException;
import java.io.IOException;

/**
 * Thread de réception de messages
 * Cette classe implémente l'interface Runnable pour être exécutée en tant que thread.
 * Elle permet de recevoir les messages envoyés par le serveur et de les afficher sur la console.
 * Elle traite également les exceptions qui peuvent être levées lors de la réception des messages
 */
class RecvThread implements Runnable {

    private final ChatClient chatClient;

    /**
     * Constructeur de la classe RecvThread
     * @param chatClient instance du client de chat
     */
    public RecvThread(ChatClient chatClient) {
        this.chatClient = chatClient;
    }

    /**
     * Méthode run
     * Cette méthode est appelée lors du lancement du thread.
     * Elle permet de recevoir les messages envoyés par le serveur et de les afficher sur la console.
     * Elle traite également les exceptions qui peuvent être levées lors de la réception des messages.
     */
    public void run() {
        try {
            while (chatClient.isbConnected()) {
                String str = chatClient.dis.readUTF(); // Lit un message envoyé par le serveur
                System.out.println(str); // Affiche le message sur la console
            }
        } catch (
                EOFException e) {
            System.out.println("Le Serveur est ferme. Au revoir");
            chatClient.disconnect(); // Déconnecte le client du serveur
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
}
