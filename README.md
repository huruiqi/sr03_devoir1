# SR03 Devoir Application de chat multi-thread en Java

### WANG Hongzhe & HU Ruiqi

## 1. Introduction

Dans ce devoir, nous avons développé une application de chat client/serveur via sockets, qui permet d'organiser des discussions entre un groupe de participants. Les chats de cette application seront affichés dans la console.

### 1.1 Concept

Notre conception est basée sur le modèle de connexion Socket prise en TD.

Nous voulons utiliser une structure de données statique non exclusive dans le serveur pour stocker les informations de connexion de chaque utilisateur. Lorsque chaque connexion est établie, un nouveau thread est créé pour la gérer, et lorsqu'il reçoit un message, il le diffuse à chaque client. Une fois que le client a quitté la connexion, le thread ferme le socket correspondant, modifie la table des informations de connexion, supprime ses propres champs, puis se termine. C'est le cycle de vie du thread serveur.

Après que chaque client ait initialisé la connexion, il vérifiera le nom d'utilisateur saisi par l'utilisateur envoyé par le serveur. Cela implique une requête sur la table d'informations de connexion. Après avoir confirmé qu'il est correct, il ouvrira deux threads pour gérer respectivement la lecture et l'écriture, et interrogera pour empêcher le blocage des deux fonctions.

Pour l'exception de connexion de socket côté serveur, nous demandons par défaut au client de quitter, de diffuser vers d'autres clients, puis de mettre à jour la table d'informations de connexion
Pour l'exception de connexion socket du client, nous fermons le serveur par défaut et terminons le processus client après en avoir informé le client.

```mermaid
sequenceDiagram
    Alice->>Server: Connect Request
    Server->>Alice: Connect Accept
    Alice->>Server: My name is Alice
    Server->>Alice: OK
    
    Bob->>Server: Connect Request
    Server->>Bob: Connect Accept
    Bob->>Server: My name is Alice
    Server->>Bob: Name used, please change a name
    Bob->>Server: My name is Bob
    Server->>Bob: OK
    Server->>Alice: Bob has joined
    
    Alice->>Server: Hello everyone
    Server->>Alice: Alice said, Hello everyone
    Server->>Bob: Alice said, Hello everyone
    
    Alice->>Server: Quit
    Server->>Bob: Alice quitted
   
```

Le serveur accepte le message du client et l'envoie à d'autres clients.

### 1.2 Objectif fixé

#### 1.2.1 Serveur

Le serveur exécute une boucle infinie pour continuer à accepter les requêtes entrantes (continue à bloquer).

1. Lorsqu'une demande de connexion arrive, le serveur stocke l'objet socket nouvellement connecté dans le tableau "clients" (à l'aide d'un type de collection qui prend en charge l'accès simultané) et démarre un thread qui intercepte l'objet socket nouvellement stocké Tous les messages envoyés dans l'objet.
2. Lorsque le serveur reçoit un message dans le socket de communication, il récupère le message puis le diffuse à tous les objets socket stockés dans le tableau "clients".
3. Si le serveur reçoit un message "exit" dans le socket de communication correspondant à n'importe quel client (disons client X), il diffuse le message suivant sur les clients connectés restants : "L'utilisateur X a quitté le chat" et libère le socket, I/ O stream et thread associé au client.
4. Le serveur gère correctement le cas des clients sortant sans prévenir.

#### 1.2.2 Client

1. Connectez-vous au serveur. Le client envoie alors son pseudonyme sur le socket de communication. s'assurer qu'il n'y a pas de doublons.
2. Implémentez deux threads. Le premier thread est utilisé pour intercepter les messages du serveur et les afficher sur la console. Le deuxième thread est utilisé pour intercepter les messages entrés par l'utilisateur et les envoyer au serveur via le socket de communication. Créez une classe qui hérite de la classe Thread pour chaque type de thread, puis redéfinissez la méthode "run()".
3. Le client gère correctement la situation où le serveur se ferme sans avertissement.

### 1.3 Utilisation théorique

- Application de chat Web anonyme sans vérification de connexion
- Outil de communication dans le réseau local au travail
- Groupes de discussion créés temporairement par les étudiants lors de discussions en classe

## 2. Explication



Dans cette parité, nous divisons notre réalisation en deux aspects : serveur et client. D'abord leur structure de données, puis leur implémentation de chaque module.

![src1](./src1.png)

### 2.1 Serveur

#### 2.1.1 Structure des données

##### CopyOnWriteArrayList

La structure de données "volatile CopyOnWriteArrayList<Client> clients = new CopyOnWriteArrayList<>()" est une liste qui permet d'accéder à des objets de type "Client" de manière concurrente et sûre en Java.

Le mot clé "volatile" indique que la variable "clients" peut être modifiée par plusieurs threads simultanément, et que chaque thread doit lire la dernière valeur écrite par un autre thread.

"CopyOnWriteArrayList" est une classe de collection dans Java qui permet une écriture concurrente sûre. Elle garantit que toutes les opérations d'écriture sur la liste sont effectuées dans une copie séparée de la liste, ce qui garantit que les opérations de lecture peuvent se faire sans verrouillage et sans risque de modification concurrente.

En somme, cette liste permet de stocker des objets "Client" de manière concurrente, tout en garantissant que les opérations de lecture se font en toute sécurité et sans conflit avec les opérations d'écriture.

#### 2.1.2 Établissment la connexion et stocker les informations utilisateur

Cette étape consiste à créer une connexion entre le serveur et le client. Le serveur doit d'abord écouter les demandes de connexion des clients. 

```java
try {
            //On boucle tant que le serveur est démarré
            while (started) {
                try {
                    //Attente d'une nouvelle connexion entrante
                    Socket s = ss.accept();
                    //Création d'un nouveau client pour la connexion entrante
                    c = new Client(this, s);
                    //Démarrage d'un nouveau thread pour le client
                    new Thread(c).start();
                    //Ajout du client à la liste des clients
                    clients.add(c);
                } catch (IOException e) {
                    //Une exception s'est produite lors de l'établissement de la connexion
                    e.printStackTrace();
                    // dis.close();
                }
            }
        }
```

Une fois que le client a envoyé une demande de connexion, le serveur stocke les informations de connexion de socket du client et les informations de l'utilisateur, et crée un nouveau thread pour gérer toutes les communications ultérieures avec lui. Ils sont tous stockés dans la classe Client. Toutes les instances de la classe client sont enregistrées dans le tableau clients et partagées entre chaque processus pour une utilisation ultérieure.

```java
    public Client(ChatServer chatServer, Socket s) {
        this.chatServer = chatServer;
        this.s = s;
        try {
            dis = new DataInputStream(s.getInputStream());
            dos = new DataOutputStream(s.getOutputStream());
            bConnected = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

En même temps, il accédera également au tableau des clients, puis appellera la méthode d'envoi de chaque objet qu'il contient pour leur envoyer un message indiquant que le client actuel est en ligne.

```java
//Si le nom d'utilisateur n'est pas déjà utilisé, il est défini sur le nom du client et un message est envoyé à tous les clients indiquant que le client a rejoint la session.
                if (flag == 0) {
                    getDos().writeUTF(str_tmp);
                    System.out.println(str_tmp + " a rejoint la conservation");
                    for (int i = 0; i < chatServer.clients.size(); i++) {
                        Client c1 = chatServer.clients.get(i);
                        if (!c1.getName().equals("")) {
                            c1.send(str_tmp + " a rejoint la conservation");
                        }
                    }
                    setName(str_tmp);
                    break;
                }
```



#### 2.1.3 Traitement des messages

Une fois qu'une connexion est établie entre le serveur et le client, le client peut envoyer des messages au serveur. Le serveur doit pouvoir traiter efficacement ces messages. Lorsqu'un message client est reçu, le thread correspondant accède au tableau des clients, puis appelle la méthode d'envoi de chaque objet qu'il contient pour leur envoyer un message.

```java
while (bConnected) {
                String str = dis.readUTF();
                for (int i = 0; i < chatServer.clients.size(); i++) {
                    Client c = chatServer.clients.get(i);
                    c.send(this.name + " a dit " + str);
                }
            }
```

```java
    public void send(String str) {
        try {
            dos.writeUTF(str);
        } catch (IOException e) {
            chatServer.clients.remove(this);
        }
    }
```



#### 2.1.4 Traitement des sorties clients

Le serveur doit également être capable de gérer correctement la sortie du client. Si un client se déconnecte du serveur, le serveur doit prendre les mesures nécessaires pour fermer la connexion et libérer toutes les ressources associées à ce client. Le serveur doit également être capable de gérer les erreurs et les exceptions qui peuvent survenir lors de la communication avec le client.

Ici, que l'utilisateur quitte normalement ou anormalement, nous adoptons une méthode unifiée pour le gérer afin d'améliorer la commodité d'utilisation.

Lorsque nous envoyons des messages dans des groupes, nous vérifions la disponibilité, interceptons l'erreur causée par l'exception de connexion socket, fermons la connexion correspondante et la supprimons du tableau clients.

```java
catch (EOFException e) {
            //Si une EOFException se produit, cela signifie que le client a quitté la session, cette méthode supprimera le client de la liste des clients et enverra un message aux autres clients indiquant que le client a quitté la session.
            if (getName().equals("")) {
                chatServer.clients.remove(this);
                System.out.println(this.name + " un client anonyme quitté la conversation");
            } else {
                System.out.println(this.name + " a quitté la conversation");
                chatServer.clients.remove(this);
                for (int i = 0; i < chatServer.clients.size(); i++) {
                    Client c = chatServer.clients.get(i);
                    c.send(this.name + " a quitté la conversation");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //Enfin, la méthode ferme tous les flux et sockets.
            try {
                if (dis != null)
                    dis.close();
                if (dos != null)
                    dos.close();
                if (s != null)
                    s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
```



### 2.2 Client

#### 2.2.1 Établissment la connexion et vérifier le nom d'utilisateur

La méthode `main` crée une instance de la classe ChatClient et appelle la méthode `launch()` pour démarrer le client. La méthode `launch()` établit d'abord une connexion au serveur, puis demande à l'utilisateur un nom d'utilisateur. Si le serveur accepte le nom d'utilisateur, le client est authentifié et les threads d'envoi et de réception sont démarrés.

La méthode `connect()` crée un nouveau socket avec l'adresse IP et le port du serveur, et initialise les flux d'entrée et de sortie pour envoyer et recevoir des données.

```java
public void launch() {
        connect(); // Établit une connexion avec le serveur
        while (true) { // Boucle infinie pour gérer la connexion
            System.out.println("Enterer votre pseudo"); // Demande à l'utilisateur d'entrer son pseudo
            String str = scanner.next(); // Lit l'entrée utilisateur
            try {
                if (str != null) {
                    if (str.equals("Quit")) { // Si l'utilisateur entre "Quit"
                        System.out.println("Bye"); // Affiche "Bye"
                        disconnect(); // Déconnecte le client
                    }
                    dos.writeUTF(str); // Envoie le pseudo au serveur via la connexion
                    if (!dis.readUTF().equals("false")) { // Si le serveur a accepté le pseudo
                        System.out.println("Welcome"); // Affiche "Welcome"
                        break; // Sort de la boucle d'authentification
                    } else {
                        System.out.println("Pseudo deja existe"); // Sinon, affiche "Pseudo deja existe"
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace(); // Affiche les erreurs d'E/S
            }
        }
        tSend.start(); // Lance le thread pour envoyer des messages
        tRecv.start(); // Lance le thread pour recevoir des messages
    }
```



#### 2.2.2 Envoi et réception de messages

Le client utilise deux threads pour gérer l'envoi et la réception des messages.

Le thread d'envoi implémenté dans la classe SendThread permet à l'utilisateur d'envoyer des messages au serveur. Il invite l'utilisateur à saisir un message, puis envoie le message au serveur via un flux de sortie. Le thread de réception implémenté dans la classe RecvThread écoute les messages envoyés depuis le serveur et les affiche sur la console.

```java
   public void run() {
        while (chatClient.isbConnected()) {
            try {
                String str = chatClient.scanner.next();

                // Si l'utilisateur entre la commande "Quit", on termine la connexion et on sort de la boucle
                if (str.equals("Quit")) {
                    System.out.println("Bye");
                    chatClient.disconnect();
                }

                // On envoie le message au serveur
                chatClient.dos.writeUTF(str);
            } catch (IOException e) {
                // En cas d'erreur, on affiche la pile d'erreurs
                e.printStackTrace();
            }

        }
    }
```

```java
public void run() {
        try {
            while (chatClient.isbConnected()) {
                String str = chatClient.dis.readUTF(); // Lit un message envoyé par le serveur
                System.out.println(str); // Affiche le message sur la console
            }
        } catch (
                EOFException e) {
            System.out.println("Le Serveur est ferme. Au revoir");
            chatClient.disconnect(); // Déconnecte le client du serveur
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }
```



#### 2.2.3 Arrêt du programme et gestion des exceptions

La méthode `disconnect()` ferme ces flux et sockets, déconnectant le client du serveur.

```java
public void disconnect() {
        try {
            dos.close(); // fermer le flux de sortie
            dis.close(); // fermer le flux d'entrée
            s.close(); // fermer le socket
            bConnected=false;
            System.exit(0); // fermer l'application
        } catch (IOException e) {
            e.printStackTrace(); // afficher la trace de l'erreur
        }
    }
```

Nous fournissons également à l'utilisateur la possibilité de quitter manuellement, lorsque l'utilisateur veut quitter, tapez "Quit", le client appelle `disconnect()` puis ferme sa connexion.

```java
if (str.equals("Quit")) { // Si l'utilisateur entre "Quit"
                        System.out.println("Bye"); // Affiche "Bye"
                        disconnect(); // Déconnecte le client
                    }
```

De plus, nous gérons également les exceptions. Lorsque nous détectons que le serveur n'est pas allumé ou que le serveur est fermé plus tôt, nous captons l'exception et appelons `disconnect()`

## 3. Demonstration

### 3.1 Préparation

#### 3.1.1 Récupération

Nous pouvons utiliser la commande `git clone` ou télécharger directement le package compressé.

```shell
git clone https://gitlab.utc.fr/huruiqi/sr03_devoir1.git
```

#### 3.1.2 Modification

Modifiez les paramètres dans les lignes de code suivantes pour permettre de définir le port du serveur que vous souhaitez ouvrir, l'adresse du serveur et le port auquel vous souhaitez vous connecter.

ChatServer.java

```java
public void start() {
        try {
            ss = new ServerSocket(8888); // Tcp的端口
            started = true;
            ...
```

ChatClient.java

```java
 public void connect() {
        try {
            s = new Socket("127.0.0.1", 8888);
            dos = new DataOutputStream(s.getOutputStream());
            dis = new DataInputStream(s.getInputStream());
            ...
```



#### 3.1.3 Exécution

##### A partir de la ligne de commande

Pour lancer ces programmes, il suffit d'entrer le progiciel correspondant, de compiler et d'exécuter le fichier selon les instructions suivantes.

**Serveur**

```shell
cd serverPackage
mkdir out
javac -d ./out ./*.java 
java -cp ./out serverPackage.ChatServer
```

**Client**

```shell
cd clientPackage
mkdir out
javac -d ./out ./*.java 
java -cp ./out clientPackage.ChatClient
```

`javac -d` permet à "javac" de sortir les fichiers de classe dans le répertoire spécifié.

`java -cp` est utilisée pour spécifier le chemin de classe lors de l'exécution d'une application Java à partir de la ligne de commande. Le chemin de classe est une liste de répertoires et/ou de fichiers jar que la machine virtuelle Java (JVM) utilise pour localiser les classes nécessaires à une application. L'utilisation est `java -cp classPath MainClass`.

##### En utilisant IDE

Ouvrez l'intégralité du projet directement à l'aide de l'IDE et exécutez la fonction principale sur la page d'édition de la classe ChatClient ou de la classe ChatServer. 

Notez que la configuration de ChatClient doit être modifiée à l'avance pour permettre à plusieurs instances de tester la capacité d'interagir avec plusieurs terminaux.

### 3.2 Utilisation

#### 3.2.1 cas normal

Vous pouvez d'abord démarrer le serveur. Lorsqu'un utilisateur se connecte, laissez l'utilisateur entrer son nom d'utilisateur. Le serveur juge si le nom d'utilisateur de l'utilisateur est répété, si tel est le cas, rappelle à l'utilisateur de le saisir à nouveau, sinon, informe l'utilisateur que l'utilisateur a été connecté et peut commencer la communication.

```
>Connected, entrer 'Quit' pour quitter
>Enterer votre pseudo
Jack
>Pseudo deja existe
>Enterer votre pseudo
Bob
>Welcome
...
```

Lorsque l'utilisateur veut quitter, il peut entrer "Quit", et le serveur recevra un rappel que l'utilisateur anonyme a quitté.

```
>Connected, entrer 'Quit' pour quitter
>Enterer votre pseudo
Quit
>Bye

>Process finished with exit code 0
```

```
...
>un client anonyme quitté la conversation
```

Lorsqu'un utilisateur entre un message, le message sera diffusé à tous les utilisateurs en ligne.

```
...
Bonjour
>Bob a dit Bonjour
```

```
...
>Bob a dit Bonjour
```

Lorsque l'utilisateur veut quitter, il peut saisir "Quit", le serveur recevra un rappel indiquant que l'utilisateur a quitté et diffusera le rappel à tous les utilisateurs en ligne.

```
...
Quit
>Bye

>Process finished with exit code 0
```

```
...
>Bob a quitté la conversation
```

#### 3.2.2 cas anormal

Lorsque le serveur n'est pas allumé et que le client veut se connecter, l'invite suivante sera reçue.

```
...
Le Serveur est ferme.

Process finished with exit code 0
```

Lorsque le serveur termine soudainement l'exécution, l'utilisateur reçoit l'invite suivante.

```
...
>Le Serveur est ferme. Au revoir

>Process finished with exit code 0
```

Lorsque l'utilisateur n'a pas défini de nom d'utilisateur et se déconnecte anormalement, le serveur reçoit l'invite suivante et la communication des autres utilisateurs ne sera pas affectée.

```
...
>un client anonyme quitté la conversation
```

Lorsqu'un utilisateur se déconnecte anormalement pendant l'utilisation, le serveur recevra l'invite suivante, et les autres utilisateurs recevront l'invite suivante, et la communication des autres utilisateurs ne sera pas affectée.

```
...
>Bob a quitté la conversation
```



## 4. Conclusion

Pour ce devoir, nous allons créer un serveur de messagerie instantanée utilisant des sockets en Java. Je trouve important de comprendre les différents composants du système et leur fonctionnement. Nous avons implémenté de nombreuses fonctions, telles que : la gestion des connexions à la banque d'informations, la création de la gestion des threads de connexion, la vérification du nom d'utilisateur, la gestion des exceptions de connexion et la diffusion de messages aux clients.

L'utilisation de threads est une bonne solution pour gérer plusieurs connexions en même temps. Cependant, il est important de s'assurer que les structures de données utilisées pour stocker les informations de connexion sont cohérentes et que toutes les modifications sont effectuées de manière sûre et synchronisée pour éviter les conflits.

La vérification du nom d'utilisateur est également une étape importante pour éviter la duplication d'utilisateurs et assurer le fonctionnement normal du logiciel. Les threads de lecture et d'écriture doivent également être surveillés pour éviter les blocages et les problèmes de synchronisation.

La gestion des exceptions de connexion est également importante pour garantir le bon fonctionnement du système, même lorsqu'il rencontre des problèmes de réseau ou des pannes matérielles. La fermeture correcte des connexions et la mise à jour appropriée des informations de connexion sont essentielles pour éviter les incohérences.

En résumé, créer un serveur de messagerie instantanée à l'aide de sockets est une tâche complexe mais passionnante. Il est important de comprendre les différents composants du système et de s'assurer que tout fonctionne correctement pour offrir une expérience utilisateur fluide et agréable.
